package com.batuhaninan.sandbox.api.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreateEmailResponse {
    private String status;
    private String emailId;
    private LocalDateTime date;
}
