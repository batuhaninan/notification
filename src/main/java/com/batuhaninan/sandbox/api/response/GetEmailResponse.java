package com.batuhaninan.sandbox.api.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GetEmailResponse {
    private String template;
    private String status;
    private String from;
    private String to;
    private String cc;
    private String bcc;
    private String subject;
    private String body;
    private LocalDateTime date;
}
