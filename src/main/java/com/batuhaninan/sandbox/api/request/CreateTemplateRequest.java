package com.batuhaninan.sandbox.api.request;

import com.batuhaninan.sandbox.dto.ArgumentTypeDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreateTemplateRequest {
    private String content;
    @Builder.Default
    private List<ArgumentTypeDTO> arguments = new ArrayList<>();
}
