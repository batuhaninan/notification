package com.batuhaninan.sandbox.api.controller;

import com.batuhaninan.sandbox.api.request.CreateEmailRequest;
import com.batuhaninan.sandbox.api.request.CreateEmailWithTemplateRequest;
import com.batuhaninan.sandbox.api.response.CreateEmailResponse;
import com.batuhaninan.sandbox.api.response.GetEmailResponse;
import com.batuhaninan.sandbox.dto.EmailDTO;
import com.batuhaninan.sandbox.service.EmailService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/email")
@RequiredArgsConstructor
public class EmailController {

    private final EmailService emailService;


    @PostMapping("/")
    private ResponseEntity<CreateEmailResponse> createEmail(@RequestBody CreateEmailRequest request) {
        return ResponseEntity
                .ok(emailService.sendEmail(EmailDTO.of(request)).toResponse());
    }

    @PostMapping("/template/")
    private ResponseEntity<CreateEmailResponse> createEmailWithTemplate(@RequestBody CreateEmailWithTemplateRequest request) {
        return ResponseEntity
                .ok(emailService.sendEmailWithTemplate(request).toResponse());
    }

    @GetMapping("/{emailId}")
    private ResponseEntity<GetEmailResponse> getEmail(@PathVariable String emailId) {
        return ResponseEntity
                .ok(emailService.getEmail(emailId));
    }

}
