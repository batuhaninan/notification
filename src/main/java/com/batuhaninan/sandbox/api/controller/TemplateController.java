package com.batuhaninan.sandbox.api.controller;

import com.batuhaninan.sandbox.api.request.CreateEmailRequest;
import com.batuhaninan.sandbox.api.request.CreateTemplateRequest;
import com.batuhaninan.sandbox.api.response.CreateEmailResponse;
import com.batuhaninan.sandbox.api.response.CreateTemplateResponse;
import com.batuhaninan.sandbox.api.response.GetTemplateResponse;
import com.batuhaninan.sandbox.service.TemplateService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/template")
@RequiredArgsConstructor
public class TemplateController {

    private final TemplateService templateService;


    @PostMapping("/")
    private ResponseEntity<CreateTemplateResponse> createTemplate(@RequestBody CreateTemplateRequest request) {
        return ResponseEntity.ok(templateService.createTemplate(request));
    }

    @GetMapping("/{templateId}")
    private ResponseEntity<GetTemplateResponse> getTemplate(@PathVariable String templateId) {
        return ResponseEntity
                .ok(templateService.getTemplate(templateId));
    }

}
