package com.batuhaninan.sandbox.repository;

import com.batuhaninan.sandbox.entity.Argument;
import org.springframework.data.repository.CrudRepository;

public interface ArgumentRepository extends CrudRepository<Argument, Long> {
}
