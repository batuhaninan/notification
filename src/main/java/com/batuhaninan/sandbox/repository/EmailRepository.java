package com.batuhaninan.sandbox.repository;

import com.batuhaninan.sandbox.entity.Email;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface EmailRepository extends CrudRepository<Email, Long> {
    Optional<Email> findEmailByUuid(String uuid);
}
