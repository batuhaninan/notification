package com.batuhaninan.sandbox.repository;

import com.batuhaninan.sandbox.entity.Template;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface TemplateRepository extends CrudRepository<Template, Long> {
    Optional<Template> findTemplateByUuid(String uuid);
}
