package com.batuhaninan.sandbox.entity;

import com.batuhaninan.sandbox.dto.ArgumentDTO;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Template {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @Column(name = "UUID")
    private String uuid;

    @Column(name = "CONTENT")
    private String content;

    @JoinColumn(name = "ARGUMENT_ID")
    @ManyToMany
    private List<Argument> arguments;

    public boolean hasArg(String argumentName) {
        return arguments.stream().anyMatch(argument -> argument.getName().equalsIgnoreCase(argumentName));
    }

    public String fillContentWithArgs(List<ArgumentDTO> args) {
        String contentToFill = this.content;
        for (ArgumentDTO argument : args) {
            if (hasArg(argument.getKey())) {
                contentToFill = contentToFill.replaceAll(String.format("\\{\\{%s\\}\\}", argument.getKey()), argument.getValue());
            }
        }
        return contentToFill;
    }
}
