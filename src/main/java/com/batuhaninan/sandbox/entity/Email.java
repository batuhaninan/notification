package com.batuhaninan.sandbox.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Email {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @Column(name = "UUID")
    private String uuid;

    @Column(name = "STATUS")
    private String status;

    @Column(name = "MAIL_FROM")
    private String from;

    @Column(name = "MAIL_TO")
    private String to;

    @Column(name = "MAIL_CC")
    private String cc;

    @Column(name = "MAIL_BCC")
    private String bcc;

    @Column(name = "SUBJECT")
    private String subject;

    @Column(name = "content")
    private String content;

    @Column(name = "DATE")
    private LocalDateTime date;

    @ManyToOne
    private Template template;
}
