package com.batuhaninan.sandbox.service;

import com.batuhaninan.sandbox.api.request.CreateEmailWithTemplateRequest;
import com.batuhaninan.sandbox.api.response.GetEmailResponse;
import com.batuhaninan.sandbox.dto.EmailDTO;
import com.batuhaninan.sandbox.dto.EmailResponseDTO;
import com.batuhaninan.sandbox.entity.Email;
import com.batuhaninan.sandbox.entity.Template;
import com.batuhaninan.sandbox.repository.ArgumentRepository;
import com.batuhaninan.sandbox.repository.EmailRepository;
import com.batuhaninan.sandbox.repository.TemplateRepository;
import com.batuhaninan.sandbox.util.StringUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class EmailService {

    private final EmailRepository emailRepository;
    private final TemplateRepository templateRepository;

    public GetEmailResponse getEmail(String emailId) {
        Email email = emailRepository.findEmailByUuid(emailId).get();

        return GetEmailResponse
                .builder()
                .subject(email.getSubject())
                .from(email.getFrom())
                .to(email.getTo())
                .cc(email.getCc())
                .bcc(email.getBcc())
                .body(email.getContent())
                .status(email.getStatus())
                .date(email.getDate())
                .build();
    }

    public EmailResponseDTO sendEmail(EmailDTO request) {

        Email email = emailRepository.save(request.toModel());

        return EmailResponseDTO
                .builder()
                .date(email.getDate())
                .emailId(email.getUuid())
                .status(email.getStatus())
                .build();
    }

    public EmailResponseDTO sendEmailWithTemplate(CreateEmailWithTemplateRequest request) {

        Template template = templateRepository.findTemplateByUuid(request.getTemplateId()).get();
        String filledTemplate = template.fillContentWithArgs(request.getArguments());

        Email email = emailRepository.save(
                Email
                        .builder()
                        .subject(request.getSubject())
                        .bcc(StringUtil.joinBy(request.getBcc(), ";"))
                        .cc(StringUtil.joinBy(request.getCc(), ";"))
                        .to(StringUtil.joinBy(request.getTo(), ";"))
                        .from(request.getFrom())
                        .uuid(UUID.randomUUID().toString())
                        .content(filledTemplate)
                        .status("SENT")
                        .date(LocalDateTime.now())
                        .build()
        );

        return EmailResponseDTO
                .builder()
                .date(email.getDate())
                .emailId(email.getUuid())
                .status(email.getStatus())
                .build();
    }

}
