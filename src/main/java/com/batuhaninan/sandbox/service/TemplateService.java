package com.batuhaninan.sandbox.service;

import com.batuhaninan.sandbox.api.request.CreateEmailWithTemplateRequest;
import com.batuhaninan.sandbox.api.request.CreateTemplateRequest;
import com.batuhaninan.sandbox.api.response.CreateTemplateResponse;
import com.batuhaninan.sandbox.api.response.GetEmailResponse;
import com.batuhaninan.sandbox.api.response.GetTemplateResponse;
import com.batuhaninan.sandbox.dto.ArgumentDTO;
import com.batuhaninan.sandbox.dto.ArgumentTypeDTO;
import com.batuhaninan.sandbox.dto.EmailDTO;
import com.batuhaninan.sandbox.dto.EmailResponseDTO;
import com.batuhaninan.sandbox.entity.Argument;
import com.batuhaninan.sandbox.entity.Email;
import com.batuhaninan.sandbox.entity.Template;
import com.batuhaninan.sandbox.repository.ArgumentRepository;
import com.batuhaninan.sandbox.repository.EmailRepository;
import com.batuhaninan.sandbox.repository.TemplateRepository;
import com.batuhaninan.sandbox.util.StringUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TemplateService {

    private final TemplateRepository templateRepository;
    private final ArgumentRepository argumentRepository;

    public GetTemplateResponse getTemplate(String templateId) {
        Template template = templateRepository.findTemplateByUuid(templateId).get();

        return GetTemplateResponse
                .builder()
                .content(template.getContent())
                .arguments(template.getArguments().stream().map(ArgumentTypeDTO::fromModel).collect(Collectors.toList()))
                .build();
    }

    public CreateTemplateResponse createTemplate(CreateTemplateRequest request) {
        List<Argument> arguments = new ArrayList<>();

        request.getArguments().forEach(argument -> {
            arguments.add(argumentRepository.save(argument.toModel()));
        });


        Template template = templateRepository.save(Template
                .builder()
                .uuid(UUID.randomUUID().toString())
                .content(request.getContent())
                .arguments(arguments)
                .build());

        return CreateTemplateResponse
                .builder()
                .templateId(template.getUuid())
                .date(LocalDateTime.now())
                .build();
    }
}
