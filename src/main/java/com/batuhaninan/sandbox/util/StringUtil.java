package com.batuhaninan.sandbox.util;

import lombok.experimental.UtilityClass;

import java.util.List;

@UtilityClass
public class StringUtil {


    public static String joinBy(List list, String delimeter) {
        return String.join(delimeter, list);
    }

}
