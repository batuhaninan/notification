package com.batuhaninan.sandbox.dto;

import com.batuhaninan.sandbox.api.response.CreateEmailResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EmailResponseDTO {
    private String status;
    private String emailId;
    private LocalDateTime date;

    public CreateEmailResponse toResponse() {
        return CreateEmailResponse
                .builder()
                .status(status)
                .emailId(emailId)
                .date(date)
                .build();
    }
}
