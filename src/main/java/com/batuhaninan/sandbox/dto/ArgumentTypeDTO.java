package com.batuhaninan.sandbox.dto;

import com.batuhaninan.sandbox.entity.Argument;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ArgumentTypeDTO {
    private String key;
    private String type;

    public Argument toModel() {
        return Argument
                .builder()
                .name(key)
                .type(type)
                .build();
    }

    public static ArgumentTypeDTO fromModel(Argument argument) {
        return ArgumentTypeDTO
                .builder()
                .key(argument.getName())
                .type(argument.getType())
                .build();
    }
}
