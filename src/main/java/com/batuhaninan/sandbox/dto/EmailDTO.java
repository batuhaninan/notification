package com.batuhaninan.sandbox.dto;

import com.batuhaninan.sandbox.api.request.CreateEmailRequest;
import com.batuhaninan.sandbox.api.request.CreateEmailWithTemplateRequest;
import com.batuhaninan.sandbox.entity.Email;
import com.batuhaninan.sandbox.util.StringUtil;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EmailDTO {
    private String from;
    @Builder.Default
    private List<String> to = new ArrayList<>();
    @Builder.Default
    private List<String> cc = new ArrayList<>();
    @Builder.Default
    private List<String> bcc = new ArrayList<>();
    private String subject;
    private String body;

    public static EmailDTO of(CreateEmailRequest request) {
        return EmailDTO
                .builder()
                .from(request.getFrom())
                .to(request.getTo())
                .cc(request.getCc())
                .bcc(request.getBcc())
                .subject(request.getSubject())
                .body(request.getBody())
                .build();
    }

    public static EmailDTO of(CreateEmailWithTemplateRequest request) {
        return EmailDTO
                .builder()
                .from(request.getFrom())
                .to(request.getTo())
                .cc(request.getCc())
                .bcc(request.getBcc())
                .subject(request.getSubject())
                .build();
    }

    public Email toModel() {
        return Email
                .builder()
                .subject(subject)
                .bcc(StringUtil.joinBy(bcc, ";"))
                .cc(StringUtil.joinBy(cc, ";"))
                .to(StringUtil.joinBy(to, ";"))
                .from(from)
                .uuid(UUID.randomUUID().toString())
                .content(body)
                .status("SENT")
                .date(LocalDateTime.now())
                .build();
    }
}
