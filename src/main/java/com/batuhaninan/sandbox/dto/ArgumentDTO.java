package com.batuhaninan.sandbox.dto;

import com.batuhaninan.sandbox.entity.Argument;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ArgumentDTO {
    private String key;
    private String value;

}
